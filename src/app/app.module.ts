import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppComponent } from './app.component';
import { SensorWidgetComponent } from './modules/sensors-list/entities/components/sensor-widget/sensor-widget.component';
import { SensorsListComponent } from './modules/sensors-list/sensors-list.component';
import { SensorPageComponent } from './modules/sensor-page/sensor-page.component';
import { RouterModule } from '@angular/router';
import { appRoutes } from './app.routes';
import { DataServiceClass } from './entities/services/data/data-service.class';
import { DataMockService } from './entities/services/data/data.mock.service';
import { SensorService } from './entities/services/sensor.service';
import { SensorStorageService } from './entities/services/sensor-storage.service';

@NgModule({
    declarations: [
        AppComponent,
        SensorWidgetComponent,
        SensorsListComponent,
        SensorPageComponent
    ],
    imports: [
        RouterModule.forRoot(appRoutes),
        BrowserModule
    ],
    providers: [
        {
            provide: DataServiceClass,
            useClass: DataMockService
        },
        SensorService,
        SensorStorageService
    ],
    bootstrap: [AppComponent]
})
export class AppModule {
}
