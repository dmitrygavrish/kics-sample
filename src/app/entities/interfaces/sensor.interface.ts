export interface Sensor {
    key: string;
    value: number;
    time: number;
    description: string;
}
