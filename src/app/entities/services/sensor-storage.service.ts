import { Injectable } from '@angular/core';
import { map, shareReplay, withLatestFrom } from 'rxjs/operators';
import { Observable, ReplaySubject } from 'rxjs';
import { Sensor } from '../interfaces/sensor.interface';
import { Dictionary } from '../interfaces/dictionary.interface';

/**
 * Storage service for sensors;
 * Allows storing sensors - provided sensors will override existing sensors;
 * Allows subscribing to array of sensors, dictionary of sensors & array of sensors' keys;
 */
@Injectable({providedIn: 'root'})
export class SensorStorageService {
    protected _sensorsKeys$$: ReplaySubject<Sensor['key'][]> = new ReplaySubject(1);
    protected _sensorsEntities$$: ReplaySubject<Dictionary<Sensor>> = new ReplaySubject(1);

    public sensorsKeys$: Observable<Sensor['key'][]> = this._sensorsKeys$$.asObservable();
    public sensorsEntities$: Observable<Dictionary<Sensor>> = this._sensorsEntities$$.asObservable();
    public sensors$: Observable<Sensor[]> = this.sensorsKeys$
        .pipe(
            withLatestFrom(this.sensorsEntities$),
            map(([keys, entities]: [Sensor['key'][], Dictionary<Sensor>]) => {
                return keys.map((key: Sensor['key']) => entities[key]);
            }),
            shareReplay(1)
        );

    public store(sensors: Sensor[]): void {
        const entities: { [key: string]: Sensor } = {};
        const keys: Sensor['key'][] = [];

        sensors.forEach((sensor: Sensor, i: number) => {
            entities[sensor.key] = sensor;
            keys[i] = sensor.key;
        });

        this._sensorsEntities$$.next(entities);
        this._sensorsKeys$$.next(keys);
    }
}
