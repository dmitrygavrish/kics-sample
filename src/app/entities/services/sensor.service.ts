import { Injectable } from '@angular/core';
import { DataServiceClass } from './data/data-service.class';
import { UpdateMode } from '../enums/update-mode.enum';
import { Observable, ReplaySubject } from 'rxjs';
import { SensorStorageService } from './sensor-storage.service';
import { Sensor } from '../interfaces/sensor.interface';

/**
 * Service, responsible for updating sensors;
 * Supports different update modes - auto & manual;
 */
@Injectable({providedIn: 'root'})
export class SensorService {
    /**
     * Id obtained from `setInterval` function, required for cancelling interval;
     */
    private _intervalId: number | null = null;

    private _currentMode: UpdateMode | null = null;

    private readonly _currentUpdateMode$$: ReplaySubject<UpdateMode> = new ReplaySubject(1);

    public readonly currentUpdateMode$: Observable<UpdateMode> = this._currentUpdateMode$$.asObservable();

    constructor(
        private readonly _dataService: DataServiceClass,
        private readonly _sensorStorageService: SensorStorageService
    ) {
    }

    public setUpdateMode(mode: UpdateMode): void {
        if (this._currentMode !== mode) {
            if (this._intervalId !== null) {
                clearInterval(this._intervalId);
            }
            if (mode === UpdateMode.Auto) {
                this._initSensorsAutoUpdate();
            }

            this._currentMode = mode;

            this._currentUpdateMode$$.next(mode);
        }
    }

    public updateSensors(): void {
        this._loadSensors();
    }

    private _initSensorsAutoUpdate(updateTimeMs: number = 1000): void {
        this._loadSensors();

        this._intervalId = setInterval(() => {
            this._loadSensors();
        }, updateTimeMs);
    }

    private _loadSensors(): void {
        this._dataService.loadSensorsData()
            .toPromise()
            .then((sensors: Sensor[]) => {
                this._sensorStorageService.store(sensors);
            });
    }
}
