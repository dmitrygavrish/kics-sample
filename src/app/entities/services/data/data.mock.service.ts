import { Injectable } from '@angular/core';
import { DataServiceClass } from './data-service.class';
import { Observable, of } from 'rxjs';
import { Sensor } from '../../interfaces/sensor.interface';
import { SensorMock } from './sensor-mock.class';
import { delay } from 'rxjs/operators';

/**
 * Mock implementation of data service;
 * Encapsulates mock data generation;
 */
@Injectable({providedIn: 'root'})
export class DataMockService extends DataServiceClass {
    private static _sensorsLength: number = 5000;
    private static _sensorsKeyPrefix: string = 'Sensor-';

    private _sensorsMocks: SensorMock[] | null = null;

    public loadSensorsData(): Observable<Sensor[]> {
        if (this._sensorsMocks === null) {
            this._generateSensors();
        }

        this._sensorsMocks.forEach((mock: SensorMock) => {
            mock.update();
        });

        return of(this._sensorsMocks.map((mock: SensorMock) => mock.get()))
            .pipe(
                delay(250)
            );
    }

    private _generateSensors(): void {
        this._sensorsMocks = [];

        for (let i: number = 0; i < DataMockService._sensorsLength; i++) {
            const key: Sensor['key'] = DataMockService._sensorsKeyPrefix + (i + 1);

            this._sensorsMocks[i] = new SensorMock(key);
        }
    }
}
