import { Observable } from 'rxjs';
import { Sensor } from '../../interfaces/sensor.interface';

/**
 * Serves as a interface & provider for data services;
 */
export abstract class DataServiceClass {
    public abstract loadSensorsData(): Observable<Sensor[]>;
}
