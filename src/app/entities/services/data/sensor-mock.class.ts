import { Sensor } from '../../interfaces/sensor.interface';
import { createRandomInteger } from '../../../utils/create-random-integer';

/**
 * Special class for simulating sensor's work - tracks sensor's individual update time
 * and performs actual update operation only after this time passes;
 * Provides simple `get/update` API;
 */
export class SensorMock {
    /**
     * Time, when last update was performed;
     */
    private _lastUpdateTime: number = Date.now();
    /**
     * How much time does it take for a sensor to update;
     */
    private _updateTime: number = this._getRandomUpdateTime(1000 * 60);
    /**
     * Ref to actual sensor;
     */
    private _sensor: Sensor;

    constructor(key: Sensor['key']) {
        this._set(this._createRandomSensor(key));
    }

    public update(): void {
        const currentTime: number = Date.now();
        const shouldUpdateAfter: number = this._lastUpdateTime + this._updateTime;

        if (currentTime >= shouldUpdateAfter) {
            this._lastUpdateTime = currentTime;

            this._set({
                ...this._sensor,
                value: this._getRandomSensorValue()
            });
        }
    }

    public get(): Sensor {
        return this._sensor;
    }
    
    private _set(sensor: Sensor): void {
        this._sensor = Object.freeze(sensor);
    }

    private _createRandomSensor(key: Sensor['key']): Sensor {
        return {
            key,
            value: this._getRandomSensorValue(),
            time: Date.now(),
            description: this._getRandomSensorDescription()
        };
    }

    private _getRandomUpdateTime(maxTimeMs: number): number {
        return createRandomInteger(100, maxTimeMs);
    }

    private _getRandomSensorValue(from: number = -50000, to: number = 50000): number {
        return createRandomInteger(from, to);
    }

    private _getRandomSensorDescription(): string {
        return 'Lorem ipsum';
    }
}
