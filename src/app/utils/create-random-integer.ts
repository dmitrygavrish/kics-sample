export function createRandomInteger(from: number, to: number): number {
    return Math.floor(Math.random() * (to + 1 - from)) + from;
}
