import { Component, OnInit } from '@angular/core';
import { UpdateMode } from './entities/enums/update-mode.enum';
import { SensorService } from './entities/services/sensor.service';
import { Observable } from 'rxjs';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
    public readonly currentMode$: Observable<UpdateMode> = this._sensorService.currentUpdateMode$;

    public readonly updateModes: UpdateMode[] = [UpdateMode.Auto, UpdateMode.Manual];

    public readonly UpdateMode: typeof UpdateMode = UpdateMode;

    constructor(
        private readonly _sensorService: SensorService
    ) {
    }

    public ngOnInit(): void {
        this._initSensors();
    }

    public onModeChange(mode: UpdateMode): void {
        this._sensorService.setUpdateMode(mode);
    }

    public onManualUpdatePress(): void {
        this._sensorService.updateSensors();
    }

    private _initSensors(): void {
        this._sensorService.setUpdateMode(UpdateMode.Auto);
    }
}
