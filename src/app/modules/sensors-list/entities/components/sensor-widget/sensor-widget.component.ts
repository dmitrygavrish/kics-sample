import { ChangeDetectionStrategy, Component, Input } from '@angular/core';

@Component({
    selector: 'app-sensor-widget',
    templateUrl: './sensor-widget.component.html',
    styleUrls: ['./sensor-widget.component.scss'],
    changeDetection: ChangeDetectionStrategy.OnPush
})
export class SensorWidgetComponent {
    @Input()
    public key: string;

    @Input()
    public value: string;
}
