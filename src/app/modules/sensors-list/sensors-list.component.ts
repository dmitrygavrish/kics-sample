import { Component } from '@angular/core';
import { Observable } from 'rxjs';
import { Sensor } from '../../entities/interfaces/sensor.interface';
import { SensorStorageService } from '../../entities/services/sensor-storage.service';

@Component({
    selector: 'app-sensors-list',
    templateUrl: './sensors-list.component.html',
    styleUrls: ['./sensors-list.component.scss']
})
export class SensorsListComponent {
    public readonly sensors$: Observable<Sensor[]> = this._sensorStorageService.sensors$;

    constructor(
        private readonly _sensorStorageService: SensorStorageService
    ) {
    }

    public trackSensorByKey(index: number, sensor: Sensor): Sensor['key'] {
        return sensor.key;
    }
}
