import { Component } from '@angular/core';
import { ActivatedRoute, ParamMap } from '@angular/router';
import { combineLatest, Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { Sensor } from '../../entities/interfaces/sensor.interface';
import { SensorStorageService } from '../../entities/services/sensor-storage.service';

@Component({
    selector: 'app-sensor-page',
    templateUrl: './sensor-page.component.html',
    styleUrls: ['./sensor-page.component.scss']
})
export class SensorPageComponent {
    private readonly _currentSensorKey$: Observable<string> = this._activatedRoute.paramMap
        .pipe(
            map((params: ParamMap) => params.get('key'))
        );

    public readonly sensor$: Observable<Sensor | null> = combineLatest([
        this._currentSensorKey$,
        this._sensorStorageService.sensorsEntities$
    ]).pipe(
        map(([currentSensorKey, sensorsEntities]: [string, { [key: string]: Sensor }]) => {
            return sensorsEntities[currentSensorKey] || null;
        })
    );

    constructor(
        private readonly _sensorStorageService: SensorStorageService,
        private readonly _activatedRoute: ActivatedRoute
    ) {
    }
}
