import { Route } from '@angular/router';
import { SensorsListComponent } from './modules/sensors-list/sensors-list.component';
import { SensorPageComponent } from './modules/sensor-page/sensor-page.component';

export const appRoutes: Route[] = [
    {
        path: '',
        component: SensorsListComponent
    },
    {
        path: 'sensor/:key',
        component: SensorPageComponent
    },
    {
        path: '**',
        redirectTo: ''
    }
];
